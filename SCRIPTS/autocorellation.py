
import math

def calculate_one_point(tau):
	R_sum = 0 
	for irow in range(len(rows)-tau):
		# irow is t 
		dx = (rows[irow+tau][1]-rows[irow][1])*(rows[irow+tau][1]-rows[irow][1]) #x
	#	dy = (rows[irow+tau][2]-rows[irow][2])*(rows[irow+tau][2]-rows[irow][2]) #y
#		dz = (rows[irow+tau][3]-rows[irow][3])*(rows[irow+tau][3]-rows[irow][3]) #z
		dR = dx
		#+dy+dz
		R_sum +=dR
	R_sum /= (len(rows)-tau) 
	return math.log(math.sqrt(R_sum),10)


rows = []
with open('end_vector.dat') as file:
	
	next(file)
	for line in file:
		map_iterator = map(float, line.strip().split())
		tokens = list(map_iterator)
		rows.append(tokens)

out = open("out_sqrt.txt", 'w')
start_x = 0
start_y = 0
for i in range(1,1000):
	#print(i)
	if i==100:
		start_x=math.log(i,10)
		start_y=calculate_one_point(i)
	out.write("%5f %8.2f\n"%(math.log(i,10),calculate_one_point(i)))
	if i==999:
		stop_x=math.log(i,10)
		stop_y=calculate_one_point(i)

out.close()	

print((stop_y-start_y)/(stop_x-start_x))
