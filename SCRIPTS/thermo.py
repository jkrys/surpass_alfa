import sys
from random import random

sys.path.append('../../src/')
from core import Plot, SvgViewport
from core.styles import color_by_name


filename = sys.argv[1]
col_x = 1
col_y = 2


def get_y_data(filename):

	data_x =[]
	data_y = []

	with open(filename) as file:
		data = file.readlines()
		x_label = data[0].strip().split()[col_x-1]
		y_label = data[0].strip().split()[col_y-1]
		for line in data[1:20000]:

			tokens = line.strip().split()
			print(tokens)
			data_x.append(float(tokens[col_x-1]))
			data_y.append(float(tokens[col_y-1]))
	return data_x,data_y




for i in range(1,len(sys.argv)):
	x,y = get_y_data(sys.argv[i])
	drawing = SvgViewport(filename+".svg", 0, 0, 1200, 1200,"white")
	pl = Plot(drawing,200,1000,200,1000,0,2000,min(y),max(y),axes_definition="UBLR")

	stroke_color = color_by_name("SteelBlue").create_darker(0.3)
	pl.axes["B"].label = "time"
	pl.axes["L"].label = "energy"
	pl.axes["L"].are_tics_inside=False
	pl.axes["L"].tics(0,5)
	#pl.axes["B"].tics_at_fraction([0.0,0.25,0.5,0.75,1.0], ["A","B","C","D","E"])
	#pl.axes["L"].tics_at_fraction([0.0,0.25,0.5,0.75,1.0], ["D","E","F","G","H"])
	for key,ax in pl.axes.items() :
		ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
		ax.tics(0,5)
	pl.draw_axes()
	pl.draw_grid()
	pl.plot_label = "Random values of x and y"
	pl.draw_plot_label()

	pl.scatter(x,y, markersize=6, markerstyle='s', title="serie-"+str(i))
drawing.close()

