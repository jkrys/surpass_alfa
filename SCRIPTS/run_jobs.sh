for chain in 15 50
do 
for dens in `cat INPUTS/density_list`
  do
  for temp in `cat INPUTS/temp_list`
    do
    DIRNAME=$dens"_"$temp
    cd OUTPUTS/$chain
    #mkdir $DIRNAME 
    cd $DIRNAME
    rm std.err std.out
    #echo $1
    qsub ../../../SCRIPTS/$1 -N $chain"_"$DIRNAME
    cd ../../../
    done
    done
done
    