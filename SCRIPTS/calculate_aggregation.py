import sys,math
from pybioshell.core.data.io import Pdb

def compute_distances(structure):
  """Dostaje strukture(model), idzie po kolei po łańcuchach i resztach
  i oblicza parami odleglosci; zwraca macierz 2D."""

  contacts_count = [[0 for i in range(structure.count_chains())] for i in range(structure.count_chains())]
  #print(contacts_count)

  for ic in range(1):
    for jc in range(ic+1,structure.count_chains()):
      #print(ic,jc)
      chain1 = structure[ic]
      chain2 = structure[jc]
      for ir in range(chain1.count_residues()):
        for jr in range(chain2.count_residues()):

          d = chain1[ir][0].distance_to(chain2[jr][0])
          if d < 11.5:
            contacts_count[ic][jc] += 1
            contacts_count[jc][ic] += 1
  #print(contacts_count)
  return contacts_count

def get_statistics(contancts):
  """Creates statistics for chains"""
  cnts_count_and_n_chains = []
  for i in range(len(contancts)):
    suma = 0
    n_chains = 0
    for var in contancts[i]:
      if var != 0:
        n_chains += 1
        suma += var
    cnts_count_and_n_chains.append([suma,n_chains])
  return cnts_count_and_n_chains

def write_to_file(lista):
  with open("cntcs.dat","w") as f:
    for i in range(len(lista)):
      f.write("%5d"%i)
      for el in lista[i]:
        f.write(" %2d %2d "%(el[0],el[1]))
      f.write("\n")

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print("Usage: python3.7 calculate_aggregation.py trajectory.pdb")
    exit()

  pdb_file = sys.argv[1]

  reader = Pdb(pdb_file,"is_not_water",False)
  structure = reader.create_structure(0)
  compute_distances(structure)
  to_write = []
  for i in range(1,reader.count_models()):
    reader.fill_structure(i,structure)
    mat = compute_distances(structure)
    to_write.append(get_statistics(mat))
  write_to_file(to_write)





  



