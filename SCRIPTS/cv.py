#!/usr/bin/env python3.7

from random import random
from core import Plot, SvgViewport
from core.styles import color_by_name
import sys, math, json


def prepare_lists(d,temp):
  wszystkie_s = []
  wszystkie_cv = []

  for T in temp:
   fname = "%s_%.1f/energy.dat" % (d,T)
   with open(fname) as f:
    s = 0
    s2 = 0
    cnt = 0
    data = f.readlines()
    for linia in data[1:] :
     tokens = linia.strip().split()
     e = float(tokens[1])
     s += e
     s2 += e*e
     cnt += 1
   s = s/cnt
   s2 = s2/(cnt)
   cv = (s2 - s*s)/(T*T)
   wszystkie_s.append(s)
   wszystkie_cv.append(cv)
  return wszystkie_cv,wszystkie_s


def prepare_to_hist(d,T):
  energy = []
  fname = "%s_%.1f/energy.dat" % (d,T)
  fname1 = "../../OUTPUTS/5/%s_%.1f/energy.dat" % (d,T)
  with open(fname) as f:
    data = f.readlines()
    for linia in data[1:] :
      tokens = linia.strip().split()
      energy.append(float(tokens[1]))

  with open(fname1) as f:
    data = f.readlines()
    for linia in data[1:] :
      tokens = linia.strip().split()
      energy.append(float(tokens[1]))

  return energy



# f = open("cv_T_d%.1f.dat" % (d), "w")
# for i in range(len(wszystkie_s)):
#  f.write("%.2f %.2f %.2f \n" %(temp[i],wszystkie_s[i],wszystkie_cv[i]))

# f.close()

def plot_param(param,d,temp,y_data):

  drawing = SvgViewport("%s_T_d%s.dat" % (param,d)+".svg", 0, 0, 1200, 1200,"white")
  pl = Plot(drawing,200,1000,200,1000,min(temp)-1,max(temp)+1,min(y_data)-1,max(y_data)+1,axes_definition="UBLR")
  print(y_data)
  stroke_color = color_by_name("SteelBlue").create_darker(0.3)
  pl.axes["B"].label = "T"
  pl.axes["L"].label = "%s"%param
  pl.axes["L"].are_tics_inside=False
  pl.axes["L"].tics(0,5)
  for key,ax in pl.axes.items() :
    ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
    ax.tics(0,5)
  pl.draw_axes()
  pl.draw_grid()
  pl.plot_label = "%s(T)"%param
  pl.draw_plot_label()

  pl.scatter(temp,y_data, markersize=6, markerstyle='s', title="serie-1")
  drawing.close()

def plot_hist(T,d,y_data):

  drawing = SvgViewport("hist_%.1f_%s.dat" % (T,d)+".svg", 0, 0, 1200, 1200,"white")
  pl = Plot(drawing,200,1000,200,1000,0,50,0,300,axes_definition="UBLR")

  stroke_color = color_by_name("SteelBlue").create_darker(0.3)
  pl.axes["B"].label = ""
  pl.axes["L"].label = "Energy"
  pl.axes["L"].are_tics_inside=False
  pl.axes["L"].tics(0,5)
  for key,ax in pl.axes.items() :
    ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 3.0
    ax.tics(0,5)
  pl.draw_axes()
  pl.draw_grid()
  pl.plot_label = "hist"
  pl.draw_plot_label()

  pl.hist(y_data,n_bins=50, markersize=6, markerstyle='s', title="serie-1")
  drawing.close()




if __name__ == "__main__":
  info = """
  USAGE:

    cv.py density temp_list e/cv

  """

  if len(sys.argv) < 3 :
    print(info)
    sys.exit(0)

  d = str(sys.argv[1])

  temp = json.loads(sys.argv[2])
  print(temp,type(temp))
  cvs,energys = prepare_lists(d,temp)
  #a = prepare_to_hist(d,3.0)
  #plot_hist(3.0,d,a)

  if len(sys.argv) == 4:
    if sys.argv[3]=="e":
      plot_param("E",d,temp,energys)
    if sys.argv[3]=="cv":
      plot_param("Cv",d,temp,cvs)
  else:
    plot_param("E",d,temp,energys)
    plot_param("Cv",d,temp,cvs)



