#!/bin/bash

INPUTS_PATH="/Users/dgront/work.git/surpass_alfa/INPUTS"
TEMPERATURES="0.80 0.90 1.00 1.10 1.20 1.40 1.50 1.60 1.80 2.00"
#TEMPERATURES="0.80 0.90"

# ---------- 
function create_folders() {
    PDB_NAME=$1
    mkdir -p $PDB_NAME
    
    for temp in $TEMPERATURES
    do
      mkdir -p $PDB_NAME/$temp
      ln -s $INPUTS_PATH/$pdb.ss2 ./$PDB_NAME/$temp/
    done
}



function average_energy() {

  grep -v "#" $1 | awk '{s+=$2;n++;} END{print s/n}'
}

for pdb in 2gb1
do
#    create_folders $pdb
    for t in $TEMPERATURES
    do
        cd $pdb/$t
        ../../runme_isothermal.sh $pdb $t > log &
        cd ../../
    done
done


