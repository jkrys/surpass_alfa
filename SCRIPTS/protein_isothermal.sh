#!/bin/bash

TARGET=$1		# JUST THE PDB CODE, e.g. 2gb1
TEMPERATURE=$2

VAL=`basename $PWD`
echo $VAL >>log

BIN_DIR="/Users/jkrys/src.git/bioshell/bin/"
BIN_DIR=""



/usr/bin/time $BIN_DIR"surpass_alfa" -verbose=INFO \
	-model:random \
	-in:ss2=$TARGET.ss2 \
	-sample:t_start=$TEMPERATURE \
	-sample:t_end=$TEMPERATURE \
	-sample:t_steps=10 \
	-sample:mc_outer_cycles=1000 \
	-sample:mc_inner_cycles=100 \
	-sample:mc_cycle_factor=1 \
	-sample:perturb:range=0.7 \
	-out:pdb:min_en=tra-min.pdb \
	-out:pdb:min_en::fraction=0.05 \
	-sample:seed=12346 \
	-sample::density=0.001
